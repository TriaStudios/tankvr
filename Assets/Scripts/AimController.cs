﻿using UnityEngine;

public class AimController : MonoBehaviour
{
    public JoystickControl rightStick;
    public JoystickControl leftStick;

    private Quaternion targetRotation;

    void Update()
    {
        //targetRotation = Quaternion.Euler(
        //    leftStick.Value * -30,
        //    rightStick.Value * 30, 0
        //);

        transform.localRotation = Quaternion.RotateTowards(transform.localRotation, targetRotation, 2);
    }
}
