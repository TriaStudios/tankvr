﻿using UnityEngine;

public class BulletController : MonoBehaviour
{
    public EnemyController[] AllEnemies;
    public GameObject effectDestroy;
    public AudioSource audioSourceDeadBullet;

    private void Start()
    {
        AllEnemies = FindObjectsOfType<EnemyController>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        foreach (var enemy in AllEnemies)
        {
            Debug.Log((transform.position - enemy.transform.position).magnitude);
            enemy.Hp -= 50 - Mathf.Clamp((transform.position - enemy.transform.position).magnitude, 0, 25) * 2;
        }
        audioSourceDeadBullet.transform.parent = null;
        Boom();
    }

    private void Boom()
    {
        audioSourceDeadBullet.transform.position = transform.position;
        audioSourceDeadBullet.Play();
        Destroy(audioSourceDeadBullet.gameObject, 5f);
        Instantiate(effectDestroy, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
