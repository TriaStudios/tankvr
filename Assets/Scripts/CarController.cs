﻿using UnityEngine;


public class CarController : MonoBehaviour
{
    public JoystickControl rightStick;
    public float rotateSpeed;
    public float speed;
    public GameObject prefabBullet;
    public Transform posSpawnBullet;
    public Transform posStartAim;
    public float forceShot;
    public AudioSource audioSourceShot;
    public GameObject effectShot;
    public AudioSource tankAudio;

    void FixedUpdate()
    {
        Vector2 speed = rightStick.Value;

        Debug.Log(speed);

        tankAudio.volume = 0.5f + Mathf.Abs(speed.y);
        transform.rotation *= Quaternion.Euler(0, speed.x * rotateSpeed, 0);
        transform.Translate(Vector3.forward * speed.y * Time.fixedDeltaTime * this.speed);
    }

    public void Shot()
    {
        Vector3 direction = posSpawnBullet.position - posStartAim.position;
        Instantiate(effectShot, posSpawnBullet.position, Quaternion.identity);
        GameObject bullet = Instantiate(prefabBullet, posSpawnBullet.position, Quaternion.identity);
        bullet.transform.up = direction;
        Rigidbody rigBullet = bullet.GetComponent<Rigidbody>();
        rigBullet.AddForce(direction * forceShot, ForceMode.Impulse);
        audioSourceShot.Play();
    }
}
