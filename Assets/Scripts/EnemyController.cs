﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    private float hp;

    public GameObject effectDead;
    public AudioSource audioSourceDead;

    private Vector3 direction;

    private int speed = 150;

    public List<WheelCollider> wheelColliders;

    public float Hp { 
        get => hp;
        set 
        { 
            hp = value;
            if (hp < 0)
            {
                Dead();
            }
        } 
    }

    private void Start()
    {
        Hp = 100;
        StartCoroutine(ChangeDirection());
    }

    private void Update()
    {
        transform.rotation = Quaternion.Euler(Vector3.Lerp(transform.rotation.eulerAngles, new Vector3(transform.rotation.eulerAngles.x, direction.y, transform.rotation.eulerAngles.z), 1 * Time.deltaTime));
        foreach (var wheel in wheelColliders)
        {
            wheel.motorTorque = speed;
            ApplyLocalPositionToVisuals(wheel);
        }
    }

    public void ApplyLocalPositionToVisuals(WheelCollider collider)
    {
        if (collider.transform.childCount == 0)
        {
            return;
        }

        Transform visualWheel = collider.transform.GetChild(0);

        collider.GetWorldPose(out Vector3 position, out Quaternion rotation);

        visualWheel.transform.position = position;
        visualWheel.transform.rotation = rotation;
    }

    IEnumerator ChangeDirection()
    {
        while (true)
        {
            yield return new WaitForSeconds(8);
            speed = -200;
            yield return new WaitForSeconds(2);
            speed = 150;
            direction = new Vector3(0, Random.Range(0, 360), 0);
        }
    }

    private void Dead()
    {
        audioSourceDead.transform.parent = null;
        audioSourceDead.transform.position = transform.position;
        audioSourceDead.Play();
        Destroy(audioSourceDead.gameObject, 5f);
        Instantiate(effectDead, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
