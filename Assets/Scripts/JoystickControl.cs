﻿using System;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

[RequireComponent(typeof(Interactable))]
[RequireComponent(typeof(InteractableHoverEvents))]
[RequireComponent(typeof(SteamVR_Skeleton_Poser))]
public class JoystickControl : MonoBehaviour
{
    public Vector2 Value
    {
        get
        {
            Vector3 euler = GetEuler(topOfJoystick, upOfJoystick) / clampOffset;
            return new Vector2(euler.z, euler.x);
        }
    }
    public bool Attached { get; private set; }

    [SerializeField] private Transform topOfJoystick;
    [SerializeField] private Transform mainRotate;
    [SerializeField] private float clampOffset;

    private readonly Hand.AttachmentFlags attachmentFlags = Hand.AttachmentFlags.DetachFromOtherHand;
    private Interactable interactable;
    private Transform upOfJoystick;

    private void Start()
    {
        interactable = GetComponent<Interactable>();
        upOfJoystick = new GameObject("UpOfJoystick").transform;
        upOfJoystick.parent = transform;
        upOfJoystick.localPosition = topOfJoystick.localPosition;
        upOfJoystick.localRotation = Quaternion.identity;
        upOfJoystick.parent = transform.parent;
        transform.parent = upOfJoystick;
        Attached = false;
    }

    protected virtual void HandHoverUpdate(Hand hand)
    {
        GrabTypes startingGrabType = hand.GetGrabStarting();

        if (interactable.attachedToHand == null && startingGrabType != GrabTypes.None)
        {
            hand.AttachObject(gameObject, startingGrabType, attachmentFlags, topOfJoystick);
            Attached = true;
        }
    }

    protected virtual void HandAttachedUpdate(Hand hand)
    {
        mainRotate.localRotation = Quaternion.Euler(GetEuler(hand.transform, upOfJoystick));

        if (hand.IsGrabEnding(gameObject))
        {
            hand.DetachObject(gameObject);
            Attached = false;
        }
    }

    private Vector3 GetEuler(Transform topOfJoystick, Transform upOfJoystick)
    {
        Vector3 nextUp = topOfJoystick.position - mainRotate.position;
        Vector3 up = upOfJoystick.position - mainRotate.position;
        Vector3 nextEuler = GetEuler(Quaternion.Inverse(upOfJoystick.rotation) * nextUp);
        Vector3 upEuler = GetEuler(Quaternion.Inverse(upOfJoystick.rotation) * up);
        return Vector3.ClampMagnitude(nextEuler - upEuler, clampOffset);
    }

    private Vector3 GetEuler(Vector3 vector)
    {
        float z = Mathf.Atan2(vector.y, vector.x) * Mathf.Rad2Deg - 90;
        float x = 90 - Mathf.Atan2(vector.y, vector.z) * Mathf.Rad2Deg;
        return new Vector3(x, 0, z);
    }

    private void FixedUpdate()
    {
        if (!Attached)
        {
            mainRotate.localRotation = Quaternion.RotateTowards(mainRotate.localRotation, Quaternion.identity, Time.fixedDeltaTime * 50);
        }
    }
}
