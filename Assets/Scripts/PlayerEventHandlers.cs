﻿using UnityEngine;
using Valve.VR.InteractionSystem;

public class PlayerEventHandlers : MonoBehaviour
{
    private Transform playerTransform;
    private Transform cameraTransform;

    private void Awake()
    {
        playerTransform = Player.instance.transform;
        cameraTransform = Camera.main.transform;
    }

    public void TeleportEyeToPoint(Transform toObject)
    {
        TeleportToPoint(toObject, Vector3.zero);
    }

    public void TeleportBodyToPoint(Transform toObject)
    {
        TeleportToPoint(toObject, Vector3.up * Player.instance.eyeHeight);
    }

    private void TeleportToPoint(Transform toObject, Vector3 offset)
    {
        playerTransform.rotation = toObject.rotation;
        playerTransform.position = CalculateRelatedPosition(toObject) + offset;
    }

    private Vector3 CalculateRelatedPosition(Transform relatedObject)
    {
        return (playerTransform.position - cameraTransform.position) + relatedObject.position;
    }
}
