﻿using UnityEngine;

public class StartLevel : MonoBehaviour
{
    public Transform tank;
    public Transform headPosition;

    void Start()
    {
        GetComponent<PlayerEventHandlers>().TeleportEyeToPoint(headPosition);
        transform.parent = tank;
    }
}
