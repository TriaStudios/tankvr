﻿using UnityEngine;

public class TreeController : MonoBehaviour
{
    private Rigidbody _rigidbody;

    private bool isDead;
    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();

        transform.forward = new Vector3(Random.Range(-180, 180), 0, 0);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag("MyTank") || collision.transform.CompareTag("TankEnemy"))
        {
            Vector3 dir = transform.position - collision.transform.position;
            _rigidbody.isKinematic = false;
            _rigidbody.AddForceAtPosition(dir.normalized * 4, transform.position + new Vector3(0, 10), ForceMode.Impulse);
            gameObject.layer = 12;
            Destroy(gameObject, 10f);
        }
    }


}
